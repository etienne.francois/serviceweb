# Auteurs.
-Etienne François
-Larose Thomas
# Description.
Le projet ici est d'avoir une API de Gestion de fiche de personnage pour les Jeux De Roles. Celle-ci est utilisée à l'aide de notre application de type "Administrateur" qui a un accès complet sur l'API (Affichage, Creation, Modification, Suppression).
# Framework.
ASP.NET CORE / C# CORE
# Niveau de maitrise.
### C#.
Niveau Intermédiaire.
### ASP .NET CORE.
Niveau Debutant.
### Choix du langage.
Nous avons décidé d'utiliser ASP.NET CORE suite au live coding, celui ci semblant etre très adapté pour la tache. De plus, ne souhaitant pas utiliser des framework Php/Java, celui-ci semblait le plus adapté. Nous étions aussi interessé par ce Framework par simple curiosité.
# Quick Start.

### Installer un certificat https.

-Sous Windows et Mac:
```sh
$ dotnet dev-certs https --trust
```
-Autres distributions:
Il est possible d'utiliser Ngix ou apache, merci de vous référer à des tutoriels.

### Restaurer les dépendances.
```sh
$ dotnet restore
```
### Lancer l'API.
Depuis la racine du projet.
```sh
$ cd PNJ_API
$ dotnet run
```
Puis rendez vous sur le lien : 
https://localhost:5001/
Vous devriez arriver sur la page d'accueil de l'API.

### Lancer l'Application.
Depuis la racine du projet.
```sh
$ cd Presentation
$ dotnet run
```
Puis rendez vous sur le lien : 
https://localhost:5002/
Vous devriez arriver sur la page d'accueil de l'Application.

# Détails de l'Architecture.
### Dossier BDD.
Celui-ci contient le fichier sqlite et un script qui permet de créer les tables et de faires des entrées de bases.

### Dossier Business.
Celui-ci contient une bibliothèque de fichiers qui sont appelés par le controller pour traiter les requêtes.

### Dossier PNJ_API.
Celui-ci contient l'API qui va recevoir les requêtes puis les rediriger vers les traitements adaptés. L'API est REST. Le seul point qui peut poser problème est la mise en cache qui n'est pas optimale.

### Dossier Presentation.
Celui ci contient l'Application qui communique et consomme l'API PNJ_API.