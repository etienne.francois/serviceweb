﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Data.Common;

namespace BDD
{   
    class Program
    {

          void command(SQLiteConnection db, string command){
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = command;
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read())
                {
                    //...
                }
            }
        }

        void createDB(SQLiteConnection db){
            try{
            command(db, "CREATE TABLE Game(" + 
                        "GameId INT PRIMARY KEY, " + 
                        "Name TEXT NOT NULL UNIQUE, " +
                        "Description TEXT);");

            command(db, "CREATE TABLE Entity(" + 
                        "EntityId INTEGER PRIMARY KEY AUTOINCREMENT, " + 
                        "Name TEXT NOT NULL, " + 
                        "GameId INT, " +
                        "img TEXT, " +
                        "FOREIGN KEY(GameId) REFERENCES Game(Id));");

            command(db, "CREATE TABLE DnDRace(" + 
                        "DnDRaceId INT PRIMARY KEY, " + 
                        "Name TEXT NOT NULL, " + 
                        "BonusStrength INT, " +
                        "BonusDexterity INT, " +
                        "BonusConstitution INT, " +
                        "BonusIntelligence INT, " +
                        "BonusWisdom INT, " +
                        "BonusCharisma INT, " +
                        "Speed INT NOT NULL, " +
                        "Size TEXT NOT NULL);");

            command(db, "CREATE TABLE DnDClass(" + 
                        "DnDClassId INT PRIMARY KEY, " + 
                        "Name TEXT NOT NULL, " + 
                        "HitDice INT, " +
                        "Armor TEXT, " +
                        "Weapons TEXT, " +
                        "SavingsThrows TEXT);");

            command(db, "CREATE TABLE DnD(" + 
                        "DnDId INT PRIMARY KEY, " + 
                        "DnDRaceId INT NOT NULL, " + 
                        "DnDClassId INT NOT NULL, " + 
                        "Level INT, " +
                        "Strength INT NOT NULL, " + 
                        "Dexterity INT NOT NULL, " + 
                        "Constitution INT NOT NULL, " + 
                        "Intelligence INT NOT NULL, " + 
                        "Wisdom INT NOT NULL, " + 
                        "Charisma INT NOT NULL, " + 
                        "FOREIGN KEY(DnDId) REFERENCES Entity(EntityId), " + 
                        "FOREIGN KEY(DnDClassId) REFERENCES DnDClass(DnDClassId), " + 
                        "FOREIGN KEY(DnDRaceId) REFERENCES DnDRaces(DnDRaceId));");

            command(db, "CREATE TABLE CoCOccupation(" + 
                        "CoCOccupationId INT PRIMARY KEY, " + 
                        "Name TEXT, " + 
                        "EDUMult INT NOT NULL, " +
                        "STRMult INT NOT NULL, " +
                        "DEXMult INT NOT NULL, " +
                        "POWMult INT NOT NULL);");

            command(db, "CREATE TABLE CoC(" + 
                        "CoCId INT PRIMARY KEY, " + 
                        "CoCOccupationId INT NOT NULL, " + 
                        "Strength INT NOT NULL, " +
                        "Constitution INT NOT NULL, " + 
                        "Dexterity INT NOT NULL, " + 
                        "Size INT NOT NULL, " + 
                        "Intelligence INT NOT NULL, " + 
                        "Power INT NOT NULL, " + 
                        "Appearance INT NOT NULL, " + 
                        "Education INT NOT NULL, " + 
                        "FOREIGN KEY(CoCId) REFERENCES Entity(EntityId), " +
                        "FOREIGN KEY(CoCOccupationId) REFERENCES CoCOccupation(CoCOccupationId));");
            }catch(Exception e){
                Console.WriteLine("Error creating tables.");
                Console.WriteLine(e);
            }
        }

        void deleteDB(SQLiteConnection db){
            try{
                command(db, "DROP TABLE DnD;");
            }catch(Exception e){
                e.ToString();
                Console.WriteLine("Error Dropping TABLE DnD.");
            }

            try{
                command(db, "DROP TABLE CoC;");
            }catch(Exception e){
                e.ToString();
                Console.WriteLine("Error Dropping Table DnDClass.");
            }

            try{
                command(db, "DROP TABLE Entity;");
            }catch(Exception e){
                e.ToString();
                Console.WriteLine("Error Dropping Table Entity.");
            }

            try{
                command(db, "DROP TABLE Game;");
            }catch(Exception e){
                e.ToString();
                Console.WriteLine("Error Dropping Table Game.");
            }

            try{
                command(db, "DROP TABLE DnDRace;");
            }catch(Exception e){
                e.ToString();
                Console.WriteLine("Error Dropping Table DnDRace.");
            }

            try{
                command(db, "DROP TABLE DnDClass;");
            }catch(Exception e){
                e.ToString();
                Console.WriteLine("Error Dropping Table DnDClass.");
            }

            try{
                command(db, "DROP TABLE CoCOccupation;");
            }catch(Exception e){
                e.ToString();
                Console.WriteLine("Error Dropping Table CoCOccupation.");
            }

        }

        void fillDB(SQLiteConnection db){
            // Filling Races
            command(db, "INSERT INTO DnDRace values(1, 'Humain', 1, 1, 1, 1, 1, 1, 9, 'M');");
            command(db, "INSERT INTO DnDRace values(2, 'Elfe', 0, 2, 0, 0, 0, 0, 9, 'M');");
            command(db, "INSERT INTO DnDRace values(3, 'Nain', 0, 0, 2, 0, 0, 0, 7, 'M');");
            command(db, "INSERT INTO DnDRace values(4, 'Gnome', 0, 0, 0, 2, 0, 0, 7, 'S');");

            // Filling Classes
            command(db, "INSERT INTO DnDClass values(1, 'Barbarian', 12, 'Light armor, Medium armor, Shields', 'Simple weapons, Martial weapons', 'Strength, Constitution');");
            command(db, "INSERT INTO DnDClass values(2, 'Cleric', 8, 'Light armor, Medium armor, Shields', 'All simple weapons', 'Wisdom, Charisma');");
            command(db, "INSERT INTO DnDClass values(3, 'Sorcerer', 6, 'None', 'Daggers, Darts, Slings, Quarterstaffs, Light crossbows', 'Constitution, Charisma');");
            command(db, "INSERT INTO DnDClass values(4, 'Rogue', 8, 'Light armor', 'Simple weapons, Hand crossbows, Longswords, Rapiers, Shortswords', 'Dexterity, Intelligence');");

            // Filling Occupations
            command(db, "INSERT INTO CoCOccupation values(1, 'Editor', 4, 0, 0, 0);");
            command(db, "INSERT INTO CoCOccupation values(2, 'Zoo Keeper', 4, 0, 0, 0);");
            command(db, "INSERT INTO CoCOccupation values(3, 'Athlete', 2, 1, 1, 0);");
            command(db, "INSERT INTO CoCOccupation values(4, 'Sailor', 2, 2, 0, 0);");
            command(db, "INSERT INTO CoCOccupation values(5, 'Teacher', 4, 0, 2, 0);");
            command(db, "INSERT INTO CoCOccupation values(6, 'Acrobat', 2, 0, 2, 0);");

            // Adding Games
            command(db, "INSERT INTO Game values(1, 'Donjons et Dragons 5e', '5ème édition de Donjons et Dragons.');");
            command(db, "INSERT INTO Game values(2, 'Call of Cthulhu', 'L Appel de Cthulhu. Inspiré de H.P. Lovecraft.');");

            // Adding DnD Entities
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Bob', 1, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Jean', 1, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Patrick', 1, 'png');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Bastien', 1, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Lorenzo', 1, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Pepito', 1, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Boromou', 1, 'jpg');");

            // Adding DnD chars
            command(db, "INSERT INTO DnD values(1, 1, 1, 1, 10, 12, 13, 14, 15, 8);");
            command(db, "INSERT INTO DnD values(2, 2, 2, 20, 12, 13, 14, 15, 8, 10);");
            command(db, "INSERT INTO DnD values(3, 3, 3, 13, 8, 10, 12, 13, 14, 15);");
            command(db, "INSERT INTO DnD values(4, 4, 4, 7, 8, 15, 14, 13, 12, 10);");
            command(db, "INSERT INTO DnD values(5, 4, 2, 9, 12, 13, 15, 14, 8, 10);");
            command(db, "INSERT INTO DnD values(6, 1, 3, 10, 12, 15, 14, 13, 10, 8);");
            command(db, "INSERT INTO DnD values(7, 2, 4, 16, 15, 14, 8, 13, 12, 10);");


            // Adding CoC Entities
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Bill', 2, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Adolf', 2, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Indiana', 2, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Claude', 2, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Bubu', 2, 'jpg');");
            command(db, "INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, 'Usain', 2, 'jpg');");

            // Adding CoC chars
            command(db, "INSERT INTO CoC values(8, 1, 8, 18, 12, 3, 5, 4, 10, 15);");
            command(db, "INSERT INTO CoC values(9, 2, 16, 10, 15, 4, 14, 6, 16, 13);");
            command(db, "INSERT INTO CoC values(10, 3, 12, 10, 10, 13, 5, 8, 4, 4);");
            command(db, "INSERT INTO CoC values(11, 4, 15, 16, 18, 12, 10, 3, 12, 13);");
            command(db, "INSERT INTO CoC values(12, 5, 3, 3, 4, 6, 8, 10, 16, 18);");
            command(db, "INSERT INTO CoC values(13, 6, 16, 11, 10, 7, 14, 6, 17, 14);");
        }

        static void Main(string[] args){
            Program p = new Program();
            SQLiteConnection db = new SQLiteConnection("DataSource=database.db3;");
            db.Open();
            p.deleteDB(db);
            p.createDB(db);
            p.fillDB(db);
        }
    }
}
