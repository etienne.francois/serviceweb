$('#OccupationForm').change(function(){
    var id = $('#OccupationForm').val();
        switch (id) {
            case '1': 
                    $('#Buff_EDU').html('4');
                    $('#Buff_STR').html('0');
                    $('#Buff_DEX').html('0');
                    $('#Buff_POW').html('0');
                break;
            case '2': 
                    $('#Buff_EDU').html('4');
                    $('#Buff_STR').html('0');
                    $('#Buff_DEX').html('0');
                    $('#Buff_POW').html('0');
                break;
    
            case '3':
                    $('#Buff_EDU').html('2');
                    $('#Buff_STR').html('1');
                    $('#Buff_DEX').html('1');
                    $('#Buff_POW').html('0');
                break;
    
            case '4': 
                    $('#Buff_EDU').html('2');
                    $('#Buff_STR').html('2');
                    $('#Buff_DEX').html('0');
                    $('#Buff_POW').html('0');
                break;
    
            case '5': 
                    $('#Buff_EDU').html('4');
                    $('#Buff_STR').html('0');
                    $('#Buff_DEX').html('2');
                    $('#Buff_POW').html('0');
                break;
    
            case '6': 
                    $('#Buff_EDU').html('2');
                    $('#Buff_STR').html('0');
                    $('#Buff_DEX').html('2');
                    $('#Buff_POW').html('0');
                break;
    
            default:
                console.log("Error selecting classes");
                break;
        } 
        });
    
        function redirect(){
            document.location.href="https://localhost:5002/Home";
        }

        var loadFile = function(event) {
            var output = document.getElementById('avatarPreview');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    
    
        $("#Edit").click(function(){
            
            var formData = new FormData();
            formData.append("game", "CoC");
    
            if(($('input[type=file]')[0].files[0])!=null){
                formData.append("image", $('input[type=file]')[0].files[0]);
            }
    
            formData.append("name",$('#PNJName').val());
    
            var stats = "";
            $( ".stats" ).each(function( index ) {
               stats += $( this ).val() +"," ;
            });
            stats = stats.substring(0, stats.length-1);
    
            formData.append("stats",stats);
    
            formData.append("role",$('#OccupationForm').val());
    
            var xhr = new XMLHttpRequest();
            xhr.open("PUT", Url, true);
            xhr.onload = function(oEvent) {
                if (xhr.status == 200) {
                alert('Creation sucessfull. Click button to go back Home');
                window.setTimeout(redirect,500)
                } else {
                alert("'" + xhr.responseText + "'");
                }
            };
            xhr.send(formData);
        });