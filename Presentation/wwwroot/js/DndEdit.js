console.log("load script ...");
$('#ClassForm').change(function(){
var id = $('#ClassForm').val();
    switch (id) {
        case '1': 
            $('#Weapons').html('Simple weapons, Martial weapons');
            $('#Armor').html('Light armor, Medium armor, Shields');
            $('#SavingThrows').html('Strength, Constitution');
            break;
        case '2': 
            $('#Weapons').html('All simple weapons') ;
            $('#Armor').html('Light armor, Medium armor, Shields');
            $('#SavingThrows').html('Wisdom, Charisma');
            break;

        case '3':
            $('#Weapons').html('Daggers, Darts, Slings, Quarterstaffs, Light crossbows') ;
            $('#Armor').html('None');
            $('#SavingThrows').html('Constitution, Charisma');
            break;

        case '4': 
            $('#Weapons').html('Simple weapons, Hand crossbows, Longswords, Rapiers, Shortswords');
            $('#Armor').html('Light armor');
            $('#SavingThrows').html('Dexterity, Intelligence');
            break;

        default:
            console.log("Error selecting classes");
            break;
    } 
});


$('#RaceForm').change(function(){
    var id = $('#RaceForm').val();
        switch (id) {
            case '1': 
                $('#Buff_STR').html('1');
                $('#Buff_DEX').html('1');
                $('#Buff_CONS').html('1');
                $('#Buff_INT').html('1');
                $('#Buff_WIS').html('1');
                $('#Buff_CHAR').html('1');
                $('#SPD').html('9');
                $('#SIZE').html('M');
                break;
            case '2': 
                $('#Buff_STR').html('0');
                $('#Buff_DEX').html('2');
                $('#Buff_CONS').html('0');
                $('#Buff_INT').html('0');
                $('#Buff_WIS').html('0');
                $('#Buff_CHAR').html('0');
                $('#SPD').html('9');
                $('#SIZE').html('M');   
                break;
    
            case '3':
                $('#Buff_STR').html('0');
                $('#Buff_DEX').html('0');
                $('#Buff_CONS').html('2');
                $('#Buff_INT').html('0');
                $('#Buff_WIS').html('0');
                $('#Buff_CHAR').html('0');
                $('#SPD').html('7');
                $('#SIZE').html('M');
                break;
    
            case '4': 
                $('#Buff_STR').html('0');
                $('#Buff_DEX').html('0');
                $('#Buff_CONS').html('0');
                $('#Buff_INT').html('0');
                $('#Buff_WIS').html('0');
                $('#Buff_CHAR').html('1');
                $('#SPD').html('7');
                $('#SIZE').html('S');
                break;
    
            default:
                console.log("Error selecting Race");
                break;
        } 
    });

    function redirect(){
        document.location.href="https://localhost:5002/Home";
    }

    var loadFile = function(event) {
        var output = document.getElementById('avatarPreview');
        output.src = URL.createObjectURL(event.target.files[0]);
    };


    $("#Edit").click(function(){
        
        var formData = new FormData();
        formData.append("game", "DnD");

        if(($('input[type=file]')[0].files[0])!=null){
            formData.append("image", $('input[type=file]')[0].files[0]);
        }

        formData.append("name",$('#PNJName').val());

        var stats = "";
        $( ".stats" ).each(function( index ) {
           stats += $( this ).val() +"," ;
        });
        stats = stats.substring(0, stats.length-1);

        formData.append("stats",stats);

        formData.append("role",$('#ClassForm').val());
        formData.append("race",$('#RaceForm').val());


        var xhr = new XMLHttpRequest();
        xhr.open("POST", Url, true);
        xhr.onload = function(oEvent) {
            if (xhr.status == 200) {
            alert('Edit sucessfull. Click button to go back Home');
            window.setTimeout(redirect,500)
            } else {
            alert("'" + xhr.responseText + "'");
            }
        };
        xhr.send(formData);
    });

    


