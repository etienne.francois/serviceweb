using System.Xml;
namespace Presentation.Models {

    public interface IPnj{
        int id {get;set;}
        string toHtml();

        string toEdit();
        void Init(XmlNode node);

        string editScript();

        string createScript();

    }

}