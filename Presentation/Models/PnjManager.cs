using System.Collections.Generic;
using System.Xml;
using System;


namespace Presentation.Models {

    public class PnjManager{
        List<IPnj> pnj ;

        public string log {get;set;}

        public PnjManager(){
            Console.WriteLine("START PARSING XML FOR MANAGER");
            pnj = new List<IPnj>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("https://localhost:5001/api/PNJ");
            foreach (XmlNode child in xmlDoc.FirstChild){
                Console.WriteLine("One more Child");
                switch(child.Name){
                case "PNJCoC":
                    CocModel coc = new CocModel();
                    (coc as IPnj).Init(child);
                    pnj.Add(coc);
                    break;
                case "PNJDnD":
                    DndModel dnd = new DndModel();
                    (dnd as IPnj).Init(child);
                    pnj.Add(dnd);
                    break;
                default:
                    break;
                }

            }
            log = toHtml();
            Console.WriteLine("END PARSING XML FOR MANAGER");
        }

        public string toHtml(){
            string result = "";
            foreach(IPnj e in pnj){
                result += e.toHtml();
            }
            return result;
        }

        public IPnj get(int id){
            foreach(IPnj e in pnj){
                if(e.id==id){
                    return e;
                }
            }
            return null;
        }



    }

}
