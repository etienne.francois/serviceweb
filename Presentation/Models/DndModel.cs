using System;

namespace Presentation.Models {

     public class DndModel : IPnj{

        int IPnj.id {get;set;}
        string name {get;set;}
        string img {get;set;}
        string ClassName {get;set;}
        string RaceName {get;set;}
        int LVL {get;set;}
        int STR {get;set;}
        int DEX {get;set;}
        int CONS {get;set;}
        int INT {get;set;}
        int WIS {get;set;}
        int CHAR {get;set;}
        string SIZE {get;set;}
        int SPD {get;set;}
        int HitDice {get;set;}
        string armor {get;set;}
        string weapons {get;set;}
        int Buff_INT {get;set;}
        int Buff_STR {get;set;}
        int Buff_DEX {get;set;}
        int Buff_CONS {get;set;}
        int Buff_WIS {get;set;}
        int Buff_CHAR {get;set;}

        string savingThrows {get;set;}

        public DndModel(){
            name = "Default";
            img  = "0.png";

            LVL=1;
            STR=1;
            DEX=1;
            CONS=1;
            INT=1;
            WIS=1;
            CHAR=1;

            ClassName = "Barbarian";
            weapons="Simple weapons, Martial weapons";
            armor="Light armor, Medium armor, Shields";
            savingThrows="Strength, Constitution";

            RaceName = "Humain";
            Buff_STR  = 1;
            Buff_DEX  = 1;
            Buff_CONS = 1;
            Buff_INT  = 1;
            Buff_WIS  = 1;
            Buff_CHAR = 1;
            SPD       = 9;
            SIZE      = "M";

        }

        string IPnj.toHtml(){
            string result = "<div class = 'pnj border border-dark' id="+(this as IPnj).id+">";
            result += "<h2>"+name+"</h2>";
            result += "<h3>" + RaceName + " " + ClassName+"</h3>";
            result += "<img src='img/dnd.png' class='logo'/>";
            result += "<a href='https://localhost:5002/Home/Edit/"+(this as IPnj).id+"'><button type='button' class='btn btn-success be'>Edit</button></a>";
            result += "<a href='https://localhost:5002/Home/Delete/"+(this as IPnj).id+"'><button type='button' class='btn btn-danger bd'>Delete</button></a>";
            result += "<ul class='columns'>";
            result += "<li class='colimg'>";
            result += "<img src='https://localhost:5001/img/"+img+"?"+DateTime.Now.ToString("yyyy’-‘MM’-‘dd’T’HH’:’mm’:’ss")+"'>";
            result += "</li>";
            result += "<li class='colstats'>";
            result += "<table class='table table-striped'>";
            result += "<tbody>";
            result += "<tr>";
            result += "<th scope='row'>LVL</th>";
            result += "<td>" + LVL + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>STR</th>";
            result += "<td>" + STR + ((Buff_STR>0) ? " (+" + Buff_STR + ")":"") + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>DEX</th>";
            result += "<td>" + DEX + ((Buff_DEX>0) ? " (+" + Buff_DEX + ")":"") + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>CON</th>";
            result += "<td>" + CONS + ((Buff_CONS>0) ? " (+" + Buff_CONS + ")":"") + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>INT</th>";
            result += "<td>" + INT + ((Buff_INT>0) ? " (+" + Buff_INT + ")":"") + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>SIZE</th>";
            result += "<td>" + SIZE + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>WIS</th>";
            result += "<td>" + WIS + ((Buff_WIS>0) ? " (+" + Buff_WIS + ")":"") + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>CHAR</th>";
            result += "<td>" + CHAR + ((Buff_CHAR>0) ? " (+" + Buff_CHAR + ")":"") + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>SPD</th>";
            result += "<td>" + SPD + "</td>";
            result += "</tr>";
            result += "</tbody>";
            result += "</table>";
            result += "</li>";
            result += "</ul>";

            result += "<table class='table table-bordered dndobjects'>";
            result += "<tbody>";
            result += "<tr>";
            result += "<th scope='row'>Weapons</th>";
            result += "<td>" + weapons + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>SavingThrows</th>";
            result += "<td>" + savingThrows + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>Armor</th>";
            result += "<td>" + armor + "</td>";
            result += "</tr>";
            result += "</tbody>";
            result += "</table>";            
            result += "</div>";
            return result;;
        }

        void IPnj.Init(System.Xml.XmlNode node){
            try{
                name = node.ChildNodes[0].InnerText;
                (this as IPnj).id   = Int32.Parse(node.ChildNodes[1].InnerText);
                img  = node.ChildNodes[1].InnerText+"."+node.ChildNodes[2].InnerText;
            }catch{
                Console.WriteLine("ERROR PARSING XML BASIC IN DND PNJ");
            }
            
            try{
                LVL  = Int32.Parse(node.ChildNodes[3].ChildNodes[0].InnerText);
                STR = Int32.Parse(node.ChildNodes[3].ChildNodes[1].InnerText);
                DEX  = Int32.Parse(node.ChildNodes[3].ChildNodes[2].InnerText);
                CONS = Int32.Parse(node.ChildNodes[3].ChildNodes[3].InnerText);
                INT  = Int32.Parse(node.ChildNodes[3].ChildNodes[4].InnerText);
                WIS  = Int32.Parse(node.ChildNodes[3].ChildNodes[5].InnerText);
                CHAR  = Int32.Parse(node.ChildNodes[3].ChildNodes[6].InnerText);
            }catch{
                Console.WriteLine("ERROR PARSING XML STATS IN DND PNJ");
            }

            try{
                ClassName    = node.ChildNodes[4].ChildNodes[0].InnerText;
                HitDice      = Int32.Parse(node.ChildNodes[4].ChildNodes[1].InnerText);
                armor        = node.ChildNodes[4].ChildNodes[2].InnerText;
                weapons      = node.ChildNodes[4].ChildNodes[3].InnerText;
                savingThrows = node.ChildNodes[4].ChildNodes[4].InnerText;
            }catch{
                Console.WriteLine("ERROR PARSING XML CLASS IN DND PNJ");
            }

            try{
            RaceName     = node.ChildNodes[5].ChildNodes[0].InnerText;
            Buff_STR     = Int32.Parse(node.ChildNodes[5].ChildNodes[1].InnerText);
            Buff_DEX     = Int32.Parse(node.ChildNodes[5].ChildNodes[2].InnerText);
            Buff_CONS    = Int32.Parse(node.ChildNodes[5].ChildNodes[3].InnerText);
            Buff_INT     = Int32.Parse(node.ChildNodes[5].ChildNodes[4].InnerText);
            Buff_WIS     = Int32.Parse(node.ChildNodes[5].ChildNodes[5].InnerText);
            Buff_CHAR    = Int32.Parse(node.ChildNodes[5].ChildNodes[6].InnerText);
            SPD          = Int32.Parse(node.ChildNodes[5].ChildNodes[7].InnerText);
            SIZE         = node.ChildNodes[5].ChildNodes[8].InnerText;

            }catch{
                Console.WriteLine("ERROR PARSING XML RACE IN DND PNJ");
            }
        }

    

     string IPnj.toEdit(){
            string result = "<div class = 'pnj border border-dark pnjEdit' id="+(this as IPnj).id+">";
            result += "<h2>"+"<input id='PNJName' type='text' name='name' value='"+name+"'/>"+"</h2>";
            result += "<ul class='columns'>";
            result += "<li class='colimg'>";
            result += "<img id='avatarPreview' src='https://localhost:5001/img/"+img+"?"+DateTime.Now.ToString("yyyy’-‘MM’-‘dd’T’HH’:’mm’:’ss")+"'>";
            result += "</li>";
            result += "<li class='colstats'>";
            result += "<table class='table table-striped table-sm'>";
            result += "<tbody>";
            result += "<tr>";
            result += "<th scope='row'>LVL</th>";
            result += "<td> <input class='stats' type='text' name='LVL' value='"+LVL+"'/></td>";
            result += "</tr><tr>";
            result += "<th scope='row'>STR</th>";
            result += "<td>" + "<input class='stats' type='text' name='STR' value='"+STR+"'/></td>";
            result += "</tr><tr>";
            result += "<th scope='row'>DEX</th>";
            result += "<td>" + "<input class='stats' type='text' name='DEX' value='"+DEX+"'/></td>";
            result += "</tr><tr>";
            result += "<th scope='row'>CON</th>";
            result += "<td>" + "<input class='stats' type='text' name='CONS' value='"+CONS+"'/></td>";
            result += "</tr><tr>";
            result += "<th scope='row'>INT</th>";
            result += "<td>" + "<input class='stats' type='text' name='INT' value='"+INT+"'/></td>";
            result += "</tr><tr>";
            result += "<th scope='row'>WIS</th>";
            result += "<td>" + "<input class='stats' type='text' name='WIS' value='"+WIS+"'/></td>";
            result += "</tr><tr>";
            result += "<th scope='row'>CHAR</th>";
            result += "<td>" + "<input class='stats' type='text' name='CHAR' value='"+CHAR+"'/></td>";
            result += "</tr><tr>";
            result += "</table>";
            result += "<FORM>";
            result += "<SELECT id='ClassForm' name='nom' size='1' class='form-control'>";
            result += "<OPTION value='1' "+((ClassName=="Barbarian") ?"selected":"")+">Barbarian";
            result += "<OPTION value='2' "+((ClassName=="Cleric") ?"selected":"")+">Cleric";
            result += "<OPTION value='3' "+((ClassName=="Sorcerer") ?"selected":"")+">Sorcerer";
            result += "<OPTION value='4' "+((ClassName=="Rogue") ?"selected":"")+">Rogue";
            result += "</SELECT>";
            result += "</FORM>";
            result += "<FORM>";
            result += "<SELECT id='RaceForm' name='nom' size='1' class='form-control'>";
            result += "<OPTION value='1' "+((RaceName=="Humain") ?"selected":"")+">Humain";
            result += "<OPTION value='2' "+((RaceName=="Elfe") ?"selected":"")+">Elfe";
            result += "<OPTION value='3' "+((RaceName=="Nain") ?"selected":"")+">Nain";
            result += "<OPTION value='4' "+((RaceName=="Gnome") ?"selected":"")+">Gnome";
            result += "</SELECT>";
            result += "</FORM>";
            result += "<input id = 'avatarModel' type='file' name='file' onchange='loadFile(event)'/>";
            result += "</li>";
            result += "</ul>";
            result += "<table class='table table-bordered multiplicateurs'>";
            result += "<thead>";
            result += "<tr>";
            result += "<th scope='col'>STR</th>";
            result += "<th scope='col'>DEX</th>";
            result += "<th scope='col'>CON</th>";
            result += "<th scope='col'>INT</th>";
            result += "<th scope='col'>WIS</th>";
            result += "<th scope='col'>CHAR</th>";
            result += "<th scope='col'>SPD</th>";
            result += "<th scope='col'>SIZE</th>";
            result += "</tr>";
            result += "</thead>";
            result += "<tbody>";
            result += "<tr>";
            result += "<th id='Buff_STR'>" + Buff_STR + "</th>";
            result += "<th id='Buff_DEX'>" + Buff_DEX + "</th>";
            result += "<th id='Buff_CONS'>" + Buff_CONS + "</th>";
            result += "<th id='Buff_INT'>" + Buff_INT + "</th>";
            result += "<th id='Buff_WIS'>" + Buff_WIS + "</th>";
            result += "<th id='Buff_CHAR'>" + Buff_CHAR + "</th>";
            result += "<th id='SPD'>" + SPD + "</th>";
            result += "<th id='SIZE'>" + SIZE + "</th>";
            result += "</tr>";
            result += "</tbody>";
            result += "</table>";

            result += "<table class='table table-bordered dndobjects'>";
            result += "<tbody>";
            result += "<tr>";
            result += "<th scope='row'>Weapons</th>";
            result += "<td id='Weapons' >" + weapons + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>SavingThrows</th>";
            result += "<td id='SavingThrows'>" + savingThrows + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>Armor</th>";
            result += "<td id = 'Armor'>" + armor + "</td>";
            result += "</tr>";
            result += "</tbody>";
            result += "</table>";            
            result += "</div>";
            return result;;
        }


        string IPnj.editScript(){
           string script = "DndEdit.js";    
           return script;
        }

        string IPnj.createScript(){
           string script = "DndCreate.js";    
           return script;
        }

    }
}

