using System;
namespace Presentation.Models {

    public class CocModel : IPnj{

        int IPnj.id {get;set;}
        string name {get;set;}
        string img {get;set;}

        int STR {get;set;}
        int CONS {get;set;}
        int DEX {get;set;}
        int SIZE {get;set;}
        int INT {get;set;}
        int POW {get;set;}
        int APP {get;set;}
        int EDU {get;set;}

        string Occupation{get;set;}

        int Buff_EDU {get;set;}
        int Buff_STR {get;set;}
        int Buff_DEX {get;set;}
        int Buff_POW {get;set;}

        public CocModel(){
            name = "Default";
            img = "0.png";
            STR=1;
            CONS=1;
            DEX=1;
            SIZE=1;
            INT=1;
            POW=1;
            APP=1;
            EDU=1;
            Occupation = "Editor";
            Buff_EDU = 4;
        }


        string IPnj.toHtml(){
            string result = "<div class = 'pnj border border-dark' id="+(this as IPnj).id +">";
            result += "<h2>"+name+"</2>";
            result += "<h3>"+Occupation+"</h3>";
            result += "<img src='img/coc.png' class='logo'/>";
            result += "<a href='https://localhost:5002/Home/Edit/"+(this as IPnj).id+"'><button type='button' class='btn btn-success be'>Edit</button></a>";
            result += "<a href='https://localhost:5002/Home/Delete/"+(this as IPnj).id+"'><button type='button' class='btn btn-danger bd'>Delete</button></a>";
            result += "<ul class='columns'>";
            result += "<li class='colimg'>";
            result += "<img src='https://localhost:5001/img/"+img+"?"+DateTime.Now.ToString("yyyy’-‘MM’-‘dd’T’HH’:’mm’:’ss")+"'>";
            result += "</li>";
            result += "<li class='colstats'>";
            result += "<table class='table table-striped'>";
            result += "<tbody>";
            result += "<tr>";

            result += "<th scope='row'>STR</th>";
            result += "<td>" + STR + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>CON</th>";
            result += "<td>" + CONS + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>DEX</th>";
            result += "<td>" + DEX + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>SIZE</th>";
            result += "<td>" + SIZE + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>INT</th>";
            result += "<td>" + INT + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>POW</th>";
            result += "<td>" + POW + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>APP</th>";
            result += "<td>" + APP + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>EDU</th>";
            result += "<td>" + EDU + "</td>";
            
            int points = 0;
            if(Buff_EDU > 0)
                points += Buff_EDU*EDU*5;
            if(Buff_DEX > 0)
                points += Buff_DEX*DEX*5;
            if(Buff_POW > 0)
                points += Buff_POW*POW*5;
            if(Buff_STR > 0)
                points += Buff_STR*STR*5;
            
            result += "</tr><tr>";

            string s = "";
            if(Buff_EDU > 0)
                s += "EDU*" + Buff_EDU + " ";
            if(Buff_DEX > 0)
                s += "DEX*" + Buff_DEX + " ";
            if(Buff_POW > 0)
                s += "POW*" + Buff_POW + " ";
            if(Buff_STR > 0)
                s += "STR*" + Buff_STR + " ";

            result += "<th scope='row'>Points d'occupations</br>" + s + "</th>";
            result += "<td>" + points + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>Points libre</th>";
            result += "<td>" + INT*10 + "</td>";

            result += "</tr>";
            result += "</tbody>";
            result += "</table>";
            result += "</li>";
            result += "</ul>";

            result += "</div>";
            return result;;
        }

        void IPnj.Init(System.Xml.XmlNode node){

            try{
                name = node.ChildNodes[0].InnerText;
                (this as IPnj).id    = Int32.Parse(node.ChildNodes[1].InnerText);
                img  = node.ChildNodes[1].InnerText+"."+node.ChildNodes[2].InnerText;
            }catch{
                Console.WriteLine("ERROR PARSING XML BASIC IN COC PNJ");
            }
            
            try{
                STR  = Int32.Parse(node.ChildNodes[3].ChildNodes[0].InnerText);
                CONS = Int32.Parse(node.ChildNodes[3].ChildNodes[1].InnerText);
                DEX  = Int32.Parse(node.ChildNodes[3].ChildNodes[2].InnerText);
                SIZE = Int32.Parse(node.ChildNodes[3].ChildNodes[3].InnerText);
                INT  = Int32.Parse(node.ChildNodes[3].ChildNodes[4].InnerText);
                POW  = Int32.Parse(node.ChildNodes[3].ChildNodes[5].InnerText);
                APP  = Int32.Parse(node.ChildNodes[3].ChildNodes[6].InnerText);
                EDU  = Int32.Parse(node.ChildNodes[3].ChildNodes[7].InnerText);
            }catch{
                Console.WriteLine("ERROR PARSING XML STATS IN COC PNJ");
            }

            try{
            Occupation = node.ChildNodes[4].ChildNodes[0].InnerText;
            Buff_EDU  = Int32.Parse(node.ChildNodes[4].ChildNodes[1].InnerText);
            Buff_STR  = Int32.Parse(node.ChildNodes[4].ChildNodes[2].InnerText);
            Buff_DEX  = Int32.Parse(node.ChildNodes[4].ChildNodes[3].InnerText);
            Buff_POW  = Int32.Parse(node.ChildNodes[4].ChildNodes[4].InnerText);
            }catch{
                Console.WriteLine("ERROR PARSING XML OCCUPATION IN COC PNJ");
            }
        }

        string IPnj.toEdit(){
            string result = "<div class = 'pnj border border-dark pnjEdit' id="+(this as IPnj).id +">";
            result += "<h2>"+"<input id='PNJName' type='text' name='name' value='"+name+"'/>"+"</h2>";
            result += "<ul class='columns'>";

            // Image
            result += "<li class='colimg'>";
            result += "<img id='avatarPreview' src='https://localhost:5001/img/"+img+"?"+DateTime.Now.ToString("yyyy’-‘MM’-‘dd’T’HH’:’mm’:’ss")+"'>";
            result += "</li>";

            // Stats, changer l'image et occupation
            result += "<li class='colstats'>";
            result += "<table class='table table-striped table-sm'>";
            result += "<tbody>";
            result += "<tr>";
            result += "<th scope='row'>STR</th>";
            result += "<td>" + "<input class='stats' type='text' name='name' value='"+STR+"'/>"+ "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>CON</th>";
            result += "<td>" +"<input class='stats type='text' name='name' value='"+CONS+"'/>"+ "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>DEX</th>";
            result += "<td>" + "<input class='stats type='text' name='name' value='"+DEX+"'/>" + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>SIZE</th>";
            result += "<td>" + "<input class='stats type='text' name='name' value='"+SIZE+"'/>" + "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>INT</th>";
            result += "<td>" +"<input class='stats type='text' name='name' value='"+INT+"'/>"+ "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>POW</th>";
            result += "<td>" +"<input class='stats type='text' name='name' value='"+POW+"'/>"+ "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>APP</th>";
            result += "<td>" + "<input class='stats type='text' name='name' value='"+APP+"'/>"+ "</td>";
            result += "</tr><tr>";
            result += "<th scope='row'>EDU</th>";
            result += "<td>" + "<input class='stats type='text' name='name' value='"+EDU+"'/>"+ "</td>";

            result += "</tr>";
            result += "</tbody>";
            result += "</table>";
            
            result += "<FORM>";
            result += "<SELECT id='OccupationForm' name='nom' size='1' class='form-control'>";
            result += "<OPTION value='1' "+((Occupation=="Editor") ?"selected":"")+">Editor";
            result += "<OPTION value='2' "+((Occupation=="Zoo Keeper") ?"selected":"")+">Zoo Keeper";
            result += "<OPTION value='3' "+((Occupation=="Athlete") ?"selected":"")+">Athlete";
            result += "<OPTION value='4' "+((Occupation=="Sailor") ?"selected":"")+">Sailor";
            result += "<OPTION value='5' "+((Occupation=="Teacher") ?"selected":"")+">Teacher";
            result += "<OPTION value='6' "+((Occupation=="Acrobat") ?"selected":"")+">Acrobat";
            result += "</SELECT>";
            result += "</FORM>";

            result += "<input id='avatarModel' type='file' name='file' onchange='loadFile(event)'/>";

            result += "</li>";
            result += "</ul>";


            result += "<h3>Multiplicateurs de l'occupation actuelle</h3>";
            result += "<table class='table table-bordered multiplicateurs'>";
            result += "<thead>";
            result += "<tr>";
            result += "<th scope='col'>EDU</th>";
            result += "<th scope='col'>STR</th>";
            result += "<th scope='col'>DEX</th>";
            result += "<th scope='col'>POW</th>";
            result += "</tr>";
            result += "</thead>";
            result += "<tbody>";
            result += "<tr>";
            result += "<th id='Buff_EDU'>" + Buff_EDU + "</th>";
            result += "<th id='Buff_STR'>" + Buff_STR + "</th>";
            result += "<th id='Buff_DEX'>" + Buff_DEX + "</th>";
            result += "<th id='Buff_POW'>" + Buff_POW + "</th>";
            result += "</tr>";
            result += "</tbody>";
            result += "</table>";

            result += "</div>";

            return result;
        }


    string IPnj.editScript(){return "CocEdit.js";}

    string IPnj.createScript(){return "CocCreate.js";}

    }


    
}