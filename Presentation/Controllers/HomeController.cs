﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using System.Xml;

namespace Presentation.Controllers
{   
    
    public class HomeController : Controller
    {
        PnjManager pnj = new PnjManager();
        public IActionResult Index()
        {
            ViewData["Manager"] = pnj;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Edit(int id)
        {
            ViewData["Pnj"] = pnj.get(id);
            return View() ;
        }

        public IActionResult Delete(int id){
            ViewData["Pnj"] = pnj.get(id);
            return View() ;
        }

        public IActionResult Create(string game){
            if(game=="CoC"){
                return View("CreateCoC");
            }
            if(game=="DnD"){
                return View("CreateDnD");
            }
            return View("index");
        }


    }
}
