﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace PNJ_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PNJController : ControllerBase
    {
        PNJ_SERVICE.GetHandler pnjGet = new PNJ_SERVICE.GetHandler();
        PNJ_SERVICE.PostHandler pnjPost = new PNJ_SERVICE.PostHandler();
        PNJ_SERVICE.PutHandler pnjPut = new PNJ_SERVICE.PutHandler();
        PNJ_SERVICE.DeleteHandler pnjDelete = new PNJ_SERVICE.DeleteHandler();
        PNJ_SERVICE.INFO info = new PNJ_SERVICE.INFO();

        // GET
        [HttpGet]
        
        public ActionResult<string> Get([FromQuery]string name,[FromQuery]string game, [FromQuery]string role)
        {     
            Console.WriteLine("GET");
            return this.Content( pnjGet.Get(name,game,role), "text/xml");  
        }

        [HttpGet("{id}")]
        public ActionResult<string> GetById(int id)
        {     
            Console.WriteLine("GET ON "+id);
            try{
                Console.WriteLine("getBy ID");
                return this.Content( pnjGet.GetById(id), "text/xml"); 
            }catch(PNJ_SERVICE.Exceptions.IdNotFoundException){
                return NotFound("Error in id : ID not found");
            } 
        }

        [HttpGet("INFO")]
        public ActionResult<string> GetInfo()
        {     
            return this.Content( info.display(), "text/html"); 
        }

        // POST api/values
        [HttpPost("{id}"), DisableRequestSizeLimit]
        public IActionResult Post(int id, [FromForm] IFormFile image, [FromForm] string name, [FromForm] int role, [FromForm] int race, [FromForm] string stats)
        {
            Console.WriteLine("POST ON"+id);
            try{
                pnjPost.Post(image, name, role, race, stats, id);
                return Ok();
            }catch(PNJ_SERVICE.Exceptions.BadFormatStatsException){
                return BadRequest("Error in Stats : Format should be 'int,int,int,..' . ");
            }catch(PNJ_SERVICE.Exceptions.NotNumberStatsException){
                return BadRequest("Error in stats : Stats should be compose of Number.");
            }catch(PNJ_SERVICE.Exceptions.NotValidGameException){
                return BadRequest("Error in game : game is not valid.");
            }catch(PNJ_SERVICE.Exceptions.InvalidImageException){
                return BadRequest("Error in image : image is invalid.");
            }catch(PNJ_SERVICE.Exceptions.RaceIndexErrorException){
                return BadRequest("Error in race : race number is invalid.");
            }catch(PNJ_SERVICE.Exceptions.RoleIndexErrorException){
                return BadRequest("Error in role : role number is invalid.");
            }catch(PNJ_SERVICE.Exceptions.LengthStatsErrorException){
                return BadRequest("Error in stats: stats length doesn't match with request.");
            }catch(PNJ_SERVICE.Exceptions.InvalidImageFormatException){
                return BadRequest("Error in image: Invalid extension.");
            }catch(PNJ_SERVICE.Exceptions.DeleteIdNotFoundException){
                return NotFound("Error in id : ID not found");
            }catch(PNJ_SERVICE.Exceptions.InvalidNumericValue){
                return BadRequest("Error in stats: All stats must be higher than 0.");
            }
        }

        // PUT api/PNJ/id
        [HttpPut, DisableRequestSizeLimit]
        public IActionResult Put([FromForm] string game, [FromForm] IFormFile image, [FromForm] string name, [FromForm] int role, [FromForm] int race, [FromForm] string stats)
        {
            Console.WriteLine("PUT");
            try{
                pnjPut.Put(game, image, name, role, race, stats);
                return Ok();
            }catch(PNJ_SERVICE.Exceptions.BadFormatStatsException){
                return BadRequest("Error in Stats : Format should be 'int,int,int,..' . ");
            }catch(PNJ_SERVICE.Exceptions.NotNumberStatsException){
                return BadRequest("Error in stats : Stats should be compose of Number.");
            }catch(PNJ_SERVICE.Exceptions.NotValidGameException){
                return BadRequest("Error in game : game is not valid.");
            }catch(PNJ_SERVICE.Exceptions.NoNameParamException){
                return BadRequest("Error in name : name is required in PUT request.");
            }catch(PNJ_SERVICE.Exceptions.InvalidImageException){
                return BadRequest("Error in image : image is invalid.");
            }catch(PNJ_SERVICE.Exceptions.RaceIndexErrorException){
                return BadRequest("Error in race : race number is invalid.");
            }catch(PNJ_SERVICE.Exceptions.RoleIndexErrorException){
                return BadRequest("Error in role : role number is invalid.");
            }catch(PNJ_SERVICE.Exceptions.LengthStatsErrorException){
                return BadRequest("Error in stats: stats length doesn't match with request.");
            }catch(PNJ_SERVICE.Exceptions.InvalidImageFormatException){
                return BadRequest("Error in image: Invalid extension.");
            }catch(PNJ_SERVICE.Exceptions.NoImageException){
                return BadRequest("Error in image: image not found.");
            }catch(PNJ_SERVICE.Exceptions.InvalidNumericValue){
                return BadRequest("Error in stats: All stats must be higher than 0.");
            }
        }

        // DELETE api/PNJ/id
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {   
            Console.WriteLine("DELETE ON"+id);
            try{
                pnjDelete.Delete(id);
                return Ok();
            }catch(PNJ_SERVICE.Exceptions.DeleteIdNotFoundException){
                return NotFound("Can't Delete : ID not found");
            }
        }
    }
}
