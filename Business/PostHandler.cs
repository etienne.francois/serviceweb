using System;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Web.Http;
using System.Data.Common;
using Microsoft.AspNetCore.Http;

namespace PNJ_SERVICE {
    public class PostHandler{
        
        public void Post(IFormFile image, String name, int role, int race, string stats, int id){
            SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
            db.OpenAndReturn();
            int gameID = -1 ;
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.Reset();
                comm.CommandText ="SELECT GameID FROM Entity WHERE EntityId="+id+";";
                IDataReader dr2=comm.ExecuteReader();
                while (dr2.Read()){
                    Console.WriteLine("ID '"+id+"' Found");
                    gameID=dr2.GetInt32(0);
                }
            } 
            if(gameID == -1){
                throw new Exceptions.IdNotFoundException();
            }        
            //update image
            switch (gameID){
                case 1:
                    Console.WriteLine("START DnD UPDATE");
                    updateDnD(role , race, stats, id, db);
                    break;
                case 2:
                    Console.WriteLine("START CoC UPDATE");
                    updateCoC(role , stats, id, db);
                    break;
                default:
                    throw new Exceptions.IdNotFoundException();
            }

            if(image!= null){
                if(Utilities.checkImage(image)){
                    updateImage(image, id, db);
                }
            }
            if(name!= null){
                updateName(name,id,db);
            }
        }  

        public void updateDnD(int role, int race, string stats, int id, SQLiteConnection db){
            if(role!=0){
                if(role > 0 && role <(CONSTANTES.GetInstance().getMaxDndClassId() +1)){
                    using (SQLiteCommand comm=db.CreateCommand()) {
                    comm.Reset();
                    comm.CommandText = "UPDATE DnD SET DnDClassId = "+role+" WHERE DnDID ="+id+";";
                    comm.ExecuteReader();
                    }  
                }else{
                    throw new Exceptions.RoleIndexErrorException();
                }
            }
            if(race!=0){
                if(race > 0 && race < (CONSTANTES.GetInstance().getMaxDndRaceId() +1) ){
                using (SQLiteCommand comm=db.CreateCommand()) {
                    comm.Reset();
                    comm.CommandText = "UPDATE DnD SET DnDRaceId = "+race+" WHERE DnDID ="+id+";";
                    comm.ExecuteReader();
                    }
                }else{
                    throw new Exceptions.RaceIndexErrorException();
                }
            }
            if(stats != null){
                int[] statsTab;
                statsTab = Utilities.convertStats(stats);
                if(statsTab.Length==7){
                   using (SQLiteCommand comm=db.CreateCommand()) {
                    comm.Reset();
                    comm.CommandText = "UPDATE DnD SET"+
                                        " Level = " +statsTab[0]+","+
                                        " Strength = " +statsTab[1]+","+ 
                                        " Dexterity = " +statsTab[2]+","+ 
                                        " Constitution = " +statsTab[3]+","+ 
                                        " Intelligence = " +statsTab[4]+","+ 
                                        " Wisdom = " +statsTab[5]+","+ 
                                        " Charisma = " +statsTab[6]+
                                        " WHERE DnDID ="+id+";";
                    comm.ExecuteReader();
                    }
                }else{
                    throw new Exceptions.LengthStatsErrorException();
                }
            }
        }   

        public void updateCoC(int role, string stats, int id, SQLiteConnection db){
            Console.WriteLine("UPDATING CoC ...");
            if(role!=0){
                if(role > 0 && role < (CONSTANTES.GetInstance().getMaxCocOccupationId() +1) ){
                    Console.WriteLine("ROLE IS OK");
                    using (SQLiteCommand comm=db.CreateCommand()) {
                    comm.Reset();
                    comm.CommandText = "UPDATE CoC SET CoCOccupationId = "+role+" WHERE CoCId ="+id+";";
                    comm.ExecuteReader();
                    }  
                }else{
                    throw new Exceptions.RoleIndexErrorException();
                }
            }

            if(stats != null){
                Console.WriteLine("STATS ARE OK");
                int[] statsTab;
                statsTab = Utilities.convertStats(stats);
                if(statsTab.Length==8){
                   using (SQLiteCommand comm=db.CreateCommand()) {
                    comm.Reset();
                    comm.CommandText = "UPDATE CoC SET"+
                                        " Strength = " +statsTab[0]+","+
                                        " Constitution = " +statsTab[1]+","+ 
                                        " Dexterity = " +statsTab[2]+","+ 
                                        " Size = " +statsTab[3]+","+ 
                                        " Intelligence = " +statsTab[4]+","+ 
                                        " Power = " +statsTab[5]+","+ 
                                        " Appearance = " +statsTab[6]+","+ 
                                        " Education = " +statsTab[7]+
                                        " WHERE CoCID ="+id+";";
                    comm.ExecuteReader();
                    }
                }else{
                    throw new Exceptions.LengthStatsErrorException();
                }
            }
        }      

        public void updateImage(IFormFile image, int id, SQLiteConnection db ){
            Utilities.imageDownload(image,id);
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.Reset();
                comm.CommandText = "UPDATE Entity SET img='"+Utilities.getImageExtension(image)+"' WHERE EntityId = "+id+";";
                comm.ExecuteReader();
            }
        }

        public void updateName(String name, int id, SQLiteConnection db ){
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.Reset();
                comm.CommandText = "UPDATE Entity SET Name='"+name+"' WHERE EntityId = "+id+";";
                comm.ExecuteReader();
            }
        }
    }
}