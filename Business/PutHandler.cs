using System;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Web.Http;
using System.Data.Common;
using Microsoft.AspNetCore.Http;

namespace PNJ_SERVICE {
    public class PutHandler{


public void Put(String game, IFormFile image, String name, int role, int race, string stats){
            if(Utilities.checkPutForm(game, image, name, role, race, stats)){
                SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
                db.Open();
                using (SQLiteCommand comm=db.CreateCommand()) {
                    switch (game){
                        case "DnD":
                            comm.CommandText ="INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, '"+name+"', 1,'"+Utilities.getImageExtension(image)+"');";
                            comm.ExecuteReader();
                            comm.Reset();
                            comm.CommandText ="SELECT MAX(EntityId) FROM Entity WHERE Entity.Name = '"+name+"' and Entity.GameId = 1;";
                            IDataReader dr1=comm.ExecuteReader();
                            while (dr1.Read()){
                               DnDPutTreatment(image, role, race, stats, dr1.GetInt32(0),db);
                            }
                            break;
                        case "CoC":
                            comm.CommandText ="INSERT INTO Entity (EntityId, Name, GameId, img) values(NULL, '"+name+"', 2,'"+Utilities.getImageExtension(image)+"');";
                            comm.ExecuteReader();
                            comm.Reset();
                            comm.CommandText ="SELECT MAX(EntityId) FROM Entity WHERE Entity.Name = '"+name+"' and Entity.GameId = 2;";
                            IDataReader dr2=comm.ExecuteReader();
                            while (dr2.Read()){
                               CoCPutTreatment(image, role, stats, dr2.GetInt32(0),db);
                            }
                            break;
                        default:
                            Console.WriteLine("Not a valid game");
                            break;
                        }
                }
            }
        }

        public void DnDPutTreatment(IFormFile image, int role, int race, string stats, int id, SQLiteConnection db){
            Utilities.imageDownload(image,id);
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.Reset();
                comm.CommandText = "INSERT INTO DnD values("+id+", "+race+", "+role+", "+stats+");";
                comm.ExecuteReader();
            }

        }
        public void CoCPutTreatment( IFormFile image, int role, string stats, int id, SQLiteConnection db){
            Utilities.imageDownload(image,id);
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.Reset();
                comm.CommandText = "INSERT INTO CoC values("+id+","+role+","+stats+");";
                comm.ExecuteReader();
            }
        }


    }
}