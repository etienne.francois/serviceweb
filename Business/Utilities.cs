using System;
using System.Data;
using System.IO;
using System.Web.Http;
using System.Data.SQLite;
using System.Data.Common;
using Microsoft.AspNetCore.Http;


namespace PNJ_SERVICE {
    static class Utilities{          
        
        static public bool checkPutForm(String game, IFormFile image, String name, int role, int race, string stats){
            int[] statsInt= convertStats(stats);
            if(statsInt==null){return false;}
            Console.WriteLine("Stats OK");
            if(name==null){throw new Exceptions.NoNameParamException();}
            Console.WriteLine("Name OK");
            if(!checkGame(game) ){return false;}
            Console.WriteLine("game OK");
            if(!checkImage(image) ){new Exceptions.InvalidImageException ();}
            Console.WriteLine("Image OK");
            if(game=="DnD" && !checkDnDCompatibility(role, race, statsInt)){return false;}
            if(game=="CoC" && !checkCoCCompatibility(role, statsInt)){return false;}
            Console.WriteLine("It's work!");
            return true;
        }

        static public int[] convertStats(string stats){
            string[] tempo ;
            try{
                tempo = stats.Split(",");
            }catch(Exception){
                    throw new Exceptions.BadFormatStatsException();
            }

            int[] result = new int[tempo.Length];
            int i = 0;
            foreach(string e in tempo){
                try{
                    result[i] = Int32.Parse(e);
                    i ++;
                }
                catch(Exception){
                    throw new Exceptions.NotNumberStatsException();
                }
                if(Int32.Parse(e)<1){
                    throw new Exceptions.InvalidNumericValue();
                }
            }
            return result;
        }

        static public bool checkGame(String game){
            switch (game){
                case "DnD":
                    return true;
                case "CoC":
                    return true;
                default:
                    throw new Exceptions.NotValidGameException();
            }
        }

        static public string getImageExtension(IFormFile image){
            if(image == null){throw new Exceptions.NoImageException();}
            String[] fileName = image.FileName.Split(".");
            return fileName[(fileName.Length-1)].ToLower();
        }

        static public bool checkImage(IFormFile image){
            string extension = getImageExtension(image);
            switch (extension){
                case "jpg":
                    return true;
                case "png":
                    return true;
                case "jpeg":
                    return true;
                default:
                    throw new Exceptions.InvalidImageFormatException();
            }
        }

        static public bool checkDnDCompatibility(int role, int race, Int32[] stats){
            if(role<1 || role> CONSTANTES.GetInstance().getMaxDndClassId() ){throw new Exceptions.RoleIndexErrorException();}
            Console.WriteLine("DnD role is OK");
            if(race<1 || race> CONSTANTES.GetInstance().getMaxDndRaceId() ){throw new Exceptions.RaceIndexErrorException();}
            Console.WriteLine("DnD race is OK");
            if(stats.Length!=7){throw new Exceptions.LengthStatsErrorException();}
             Console.WriteLine("DnD stats are OK");
            return true;
        }

        static public bool checkCoCCompatibility(int role, Int32[] stats){
            if(role<1 || role> CONSTANTES.GetInstance().getMaxCocOccupationId() ){throw new Exceptions.RoleIndexErrorException();}
             Console.WriteLine("CoC role is OK");
            if(stats.Length!=8){throw new Exceptions.LengthStatsErrorException();}
            Console.WriteLine("CoC stats are OK");
            return true;
        }

        static public void imageDownload(IFormFile image, int name){
            if(image == null){ throw new Exceptions.NoImageException();}
            var filename ="wwwroot/img/"+name+"."+getImageExtension(image);
            using (FileStream fs = System.IO.File.Create(filename)) 
            { 
                image.CopyTo(fs); 
                fs.Flush(); 
            }
        }

    }

}