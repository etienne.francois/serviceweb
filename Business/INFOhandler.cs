using System.Data;
using System.IO;
using System.Data.SQLite;

namespace PNJ_SERVICE {
    public class INFO{
        static string HEADER = "<!DOCTYPE html>"+
                                        "<html lang='en'>"+
                                        "<head>"+
                                        "<title>PNJ API</title>"+
                                        "<!-- Meta -->"+
                                        "<meta charset='utf-8'>"+
                                        "<meta http-equiv='X-UA-Compatible' content='IE=edge'>"+
                                        "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"+
                                        "<meta name='description' content=''>"+
                                        "<meta name='author' content=''>"+
                                        "<link rel='shortcut icon' href='../../favicon.ico'>"+
                                        "<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>"+
                                        "<!-- FontAwesome JS -->"+
                                        "<script defer src='https://use.fontawesome.com/releases/v5.7.2/js/all.js' integrity='sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP' crossorigin='anonymous'></script>"+
                                        "<!-- Global CSS -->"+
                                        "<link rel='stylesheet' href='../../assets/plugins/bootstrap/css/bootstrap.min.css'>"+
                                        "<!-- Plugins CSS -->"+
                                        "<link rel='stylesheet' href='../../assets/plugins/elegant_font/css/style.css'>"+
                                        "<!-- Theme CSS -->"+
                                        "<link id='theme-style' rel='stylesheet' href='../../assets/css/styles.css'>"+
                                        "</head>";


        static string FOOTER = "<footer id='footer' class='footer text-center'>"+
                                "<div class='container'>"+
                                "<small class='copyright'>On remercie <a href='https://themes.3rdwavemedia.com/' target='_blank'>Xiaoying Riley</a> car on avait la flemme de designer une page.</small>"+
                                "</div><!--//container-->"+
                                "</footer><!--//footer-->";

            static string HEADER2 = "<header id='header' class='header'>"+
                                    "<div class='container'>"+
                                    "<div class='branding'>"+
                                    "<h1 class='logo'>"+
                                    "<a href='../../index.html'>"+
                                    "<span aria-hidden='true' class='icon_documents_alt icon'></span>"+
                                    "<span class='text-highlight'>PNJ</span><span class='text-bold'>API</span>"+
                                    "</a></h1></div><!--//branding-->"+
                                    "<ol class='breadcrumb'><li class='breadcrumb-item'><a href='../../index.html'>Home</a></li></ol>"+
                                    "</div><!--//container--></header><!--//header-->";
        public string contentClass(){
            SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
            db.Open();
            string result = "<h2 class='section-title'>Classes de Donjons et Dragons</h2><table><tr><td>ID</td><td>Designation</td></tr>";
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = "SELECT DnDClassId,Name FROM DnDClass; ";
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){ 
                    result += "<tr>";
                    result += "<td>"+dr.GetInt32(0)+"</td>";
                    result += "<td>"+dr.GetString(1)+"</td>";
                    result += "</tr>";
                }
            }
            return result +"</table>";
        }

        public string contentRace(){
            SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
            db.Open();
            string result = "<h2 class='section-title'>Race de Donjons et Dragons</h2><table><tr><td>ID</td><td>Designation</td></tr>";
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = "SELECT DnDRaceId,Name FROM DnDRace; ";
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){ 
                    result += "<tr>";
                    result += "<td>"+dr.GetInt32(0)+"</td>";
                    result += "<td>"+dr.GetString(1)+"</td>";
                    result += "</tr>";
                }
            }
            return result +"</table>";
        } 

        public string contentOccupation(){
            SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
            db.Open();
            string result = "<h2 class='section-title'>Occupation de Call of Cthulhu </h2><table><tr><td>ID</td><td>Designation</td></tr>";
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = "SELECT CoCOccupationId,Name FROM CoCOccupation; ";
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){ 
                    result += "<tr>";
                    result += "<td>"+dr.GetInt32(0)+"</td>";
                    result += "<td>"+dr.GetString(1)+"</td>";
                    result += "</tr>";
                }
            }
            return result +"</table>";
        } 

        
        public string display(){
                return "<html>"+HEADER+"<body>"+HEADER2+contentClass()+contentRace()+contentOccupation()+FOOTER+"</body></html>";
        }
    }
}
