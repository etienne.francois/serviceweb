using System;
using System.Data;
using System.IO;
using System.Data.SQLite;

namespace PNJ_SERVICE{
   sealed class CONSTANTES
   {
      int maxDndClassId;
      int maxDndRaceId;
      int maxCocOccupationId;
      static CONSTANTES singleton = null;
      static object synchro = new object();

      SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
      CONSTANTES()
      {
            db.Open();
            updateDndClass();
            updateDndRace();
            updateCocOccupation();
      }

      private void updateDndClass(){
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = "SELECT count(*) FROM DnDClass;";
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){
                    maxDndClassId=dr.GetInt32(0);
               }
         }
      }   

      private void updateDndRace(){
         using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = "SELECT count(*) FROM DnDRace;";
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){
                    maxDndRaceId=dr.GetInt32(0);
            }
         }   
      }

      private void updateCocOccupation(){
         using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = "SELECT count(*) FROM CoCOccupation;";
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){
                    maxCocOccupationId=dr.GetInt32(0);
            }
         }
      }

      public static CONSTANTES GetInstance()
      {
         if (singleton == null)
         {
            lock (synchro)
            {
               if (singleton == null)
               {
                  singleton = new CONSTANTES();
               }
            }
         }
         return singleton;
      }
      public int getMaxDndRaceId()
      {
        Console.WriteLine(maxDndRaceId);
         return maxDndRaceId;
      }

      public int getMaxDndClassId()
      {
          Console.WriteLine(maxDndClassId);
         return maxDndClassId;
      }

      public int getMaxCocOccupationId()
      {
          Console.WriteLine(maxCocOccupationId);
         return maxCocOccupationId;
      }
   }

}