using System.Data;
using System.IO;
using System.Data.SQLite;

namespace PNJ_SERVICE {
    public class GetHandler{

        //get request and utilities//delete request and utilities
        static string DNDREQ ="SELECT * FROM DnD INNER JOIN DnDClass ON DND.DnDClassId = DnDClass.DnDClassId INNER JOIN DnDRace ON DnD.DnDRaceId = DnDRace.DnDRaceId INNER JOIN Entity ON DnD.DnDId = Entity.EntityId ";
        static string COCREQ = "SELECT * FROM CoC INNER JOIN CoCOccupation ON CoC.CoCOccupationId = CoCOccupation.CoCOccupationId INNER JOIN Entity ON CoC.CoCId = Entity.EntityId ";

        public string Get(string name, string game, string role){
            string DNDadds ="";
            string COCadds ="";

            if(!(name==null)){
                DNDadds += "Entity.Name LIKE " + "'%" + name + "%'"; 
                COCadds += "Entity.Name LIKE " + "'%" + name + "%'"; 
            }

            if(name != null && role != null){
                DNDadds += " AND "; 
                COCadds += " AND "; 
            }

            if(!(role==null)){
                DNDadds += "DnDRace.Name LIKE " + "'%" + role + "%'"; 
                COCadds += "CoCOccupation.Name LIKE " + "'%" + role + "%'"; 
            }

            if(DNDadds.Length>0){
                DNDadds = "WHERE " + DNDadds; // add where 
                COCadds = "WHERE " + COCadds; 
            }

            string result = "<result>";
            if(game=="DnD"){
                result += DnDTreatment(DNDREQ + DNDadds + ";");
            }else if(game=="CoC"){
                result += CocTreatment(COCREQ + COCadds + ";");
            } else{
                 result += CocTreatment(COCREQ + COCadds + ";");
                 result += DnDTreatment(DNDREQ + DNDadds + ";");
            }           
            result += "</result>";
            return result;
        }

        public string GetById(int id){
            string result = "<result>";
             SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
            db.Open();
            int game =0 ;
            using (SQLiteCommand comm=db.CreateCommand()){
            comm.CommandText ="SELECT GameId From Entity WHERE EntityId ="+id+";";
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){   
                    game = dr.GetInt32(0);
                }
            }
            switch (game){
                case 1:
                    result += DnDTreatment(DNDREQ + "WHERE EntityId="+id+ ";");
                    break;
                case 2:
                    result += CocTreatment(COCREQ + "WHERE EntityId="+id+ ";");
                    break;
                default:
                    throw new Exceptions.IdNotFoundException();
            }
            
            result += "</result>";
            return result;
        }
        

        public string CocTreatment(string command){
            SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
            db.Open();
            string result = "";
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = command;
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){   
                    result+="<PNJCoC>";
                    result+="<Name>"+dr.GetString(17)+"</Name>";
                    result+="<ID>"+dr.GetInt64(16)+"</ID>";
                    result+="<IMG>"+dr.GetString(19)+"</IMG>";
                    result+="<Stats>";
                        result+="<STR>"+dr.GetInt64(2)+"</STR>";
                        result+="<CONS>"+dr.GetInt64(3)+"</CONS>";
                        result+="<DEX>"+dr.GetInt64(4)+"</DEX>";
                        result+="<SIZE>"+dr.GetInt64(5)+"</SIZE>";
                        result+="<INT>"+dr.GetInt64(6)+"</INT>";
                        result+="<POW>"+dr.GetInt64(7)+"</POW>";
                        result+="<APP>"+dr.GetInt64(8)+"</APP>";
                        result+="<EDU>"+dr.GetInt64(9)+"</EDU>";
                    result+="</Stats>";
                    result+="<Occupation>";
                        result+="<Name>"+dr.GetString(11)+"</Name>";
                        result+="<EDU>"+dr.GetInt64(12)+"</EDU>";
                        result+="<STR>"+dr.GetInt64(13)+"</STR>";
                        result+="<DEX>"+dr.GetInt64(14)+"</DEX>";
                        result+="<POW>"+dr.GetInt64(15)+"</POW>";
                    result+="</Occupation>";
                    
                    result+="</PNJCoC>";
                }
            }
            return result;
        }

        public string DnDTreatment(string command){
            SQLiteConnection db = new SQLiteConnection(@"DataSource=../BDD/database.db3;");
            db.Open();
            string result = "";
            using (SQLiteCommand comm=db.CreateCommand()) {
                comm.CommandText = command;
                IDataReader dr=comm.ExecuteReader();
                while (dr.Read()){   
                    result+="<PNJDnD>";
                    result+="<Name>"+dr.GetString(27)+"</Name>";
                    result+="<ID>"+dr.GetInt64(26)+"</ID>";
                    result+="<IMG>"+dr.GetString(29)+"</IMG>";
                    result+="<Stats>";
                        result+="<LVL>"+dr.GetInt64(3)+"</LVL>";
                        result+="<STR>"+dr.GetInt64(4)+"</STR>";
                        result+="<DEX>"+dr.GetInt64(5)+"</DEX>";
                        result+="<CONS>"+dr.GetInt64(6)+"</CONS>";
                        result+="<INT>"+dr.GetInt64(7)+"</INT>";
                        result+="<WIS>"+dr.GetInt64(8)+"</WIS>";
                        result+="<CHAR>"+dr.GetInt64(9)+"</CHAR>";
                    result+="</Stats>";

                    result+="<Class>";
                        result+="<ClassName>"+dr.GetString(11)+"</ClassName>";
                        result+="<HitDice>"+dr.GetInt64(12)+"</HitDice>";
                        result+="<Armor>"+dr.GetString(13)+"</Armor>";
                        result+="<Weapons>"+dr.GetString(14)+"</Weapons>";
                        result+="<SavingsThrows>"+dr.GetString(15)+"</SavingsThrows>";
                    result+="</Class>";

                    result+="<Race>";
                        result+="<RaceName>"+dr.GetString(17)+"</RaceName>";
                        result+="<STR>"+dr.GetInt64(18)+"</STR>";
                        result+="<DEX>"+dr.GetInt64(19)+"</DEX>";
                        result+="<CONS>"+dr.GetInt64(20)+"</CONS>";
                        result+="<INT>"+dr.GetInt64(21)+"</INT>";
                        result+="<WIS>"+dr.GetInt64(22)+"</WIS>";
                        result+="<CHAR>"+dr.GetInt64(23)+"</CHAR>";
                        result+="<SPD>"+dr.GetFloat(24)+"</SPD>";
                        result+="<SIZE>"+dr.GetString(25)+"</SIZE>";
                    result+="</Race>";
                    
                    
                    result+="</PNJDnD>";                
                }
                
            }
            return result;
        }
    }
}