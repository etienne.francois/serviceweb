namespace PNJ_SERVICE.Exceptions
{
    public class DeleteIdNotFoundException : System.Exception{}

    public class BadFormatStatsException : System.Exception{}

    public class NotNumberStatsException : System.Exception{}

    public class NotValidGameException : System.Exception{}

    public class NoNameParamException : System.Exception{}

    public class InvalidImageException : System.Exception{}

    public class RoleIndexErrorException : System.Exception{}

    public class RaceIndexErrorException : System.Exception{}

    public class LengthStatsErrorException : System.Exception{}

    public class InvalidImageFormatException : System.Exception{}

    public class IdNotFoundException : System.Exception{}

    public class NoImageException : System.Exception{}

    public class InvalidNumericValue : System.Exception{}


}